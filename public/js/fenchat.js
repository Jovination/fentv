var socket = io.connect('http://node1.transutil.com:8081');
socket.on('connect', function () {
	console.log('connected');
});
var nick,colors = {
    white: '#fff',
    black: '#000',
    dark_blue: '\\u000302',
    dark_green: '\\u000303',
    light_red: 'lightred',
    dark_red: 'darkred',
    magenta: '\\u000306',
    orange: '\\u000307',
    yellow: '\\u000308',
    light_green: '\\u000309',
    cyan: '\\u000310',
    light_cyan: '\\u000311',
    light_blue: '\\u000312',
    light_magenta: '#ff42f9',
    gray: '\\u000314',
    light_gray: '\\u000315',
    reset: '#fff',
},channelList=[];
function checkClientCode(message){
	var codes = {
		emote: '\/me '
	},exp,code;
	for(var index in codes){
		if(codes.hasOwnProperty(index)){
			exp = new RegExp('('+codes[index]+')');
			if(exp.test(message)){
				code = index;
				message = message.replace(exp,'');
				return {message:message,code:code};
			}
		}
	}
	return {message:message,code:false};
}
function replaceURLWithHTMLLinks(text) {
    var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/i;
    return text.replace(exp,'<a href="$1" target="_blank">$1</a>'); 
}
function sendMSG(){
	var msg = jQuery('.msg_fenchat').val();
	jQuery('.msg_fenchat').val('');
	if(msg!=''){
		socket.emit('fenchat-message',{msg:msg});
		var message = '<tr>',parse = checkClientCode(msg);

		message += '<td class="time"><div>'+moment().format('h:mm:ss a')+'</div></td>';
		if(parse.code=='emote'){
			message += '<td class="nick">*</td>';
			message += '<td class="message">'+nick+' '+replaceURLWithHTMLLinks(parse.message)+'</td>';
		}else{
			message += '<td class="nick">'+nick+'</td>';
			message += '<td class="message">'+replaceURLWithHTMLLinks(msg)+'</td>';
		}
		message += '</tr>';
		jQuery('.channel-text table tbody').prepend(message);
	}
}
function joinFenchat(){
	jQuery('.submit_msg').off('click').on('click',function(){
		var nick = jQuery('.msg_fenchat').val();
		jQuery('.msg_fenchat').val('');
		jQuery('.msg_fenchat').attr('placeholder','Waiting.....');
		socket.emit('join-fenchat',{nick:nick});
		jQuery(this).text('Joining');
		jQuery(this).off('click');
	});
	jQuery('.msg_fenchat').off('keyup').on('keyup',function(event){
		var key = event.keyCode || event.which;
		if(key===13){
			var nick = jQuery('.msg_fenchat').val();
			jQuery('.msg_fenchat').val('');
			jQuery('.msg_fenchat').attr('placeholder','Waiting.....');
			socket.emit('join-fenchat',{nick:nick});
			jQuery(this).text('Joining');
			jQuery(this).off('keyup');
		}
	});
}
function msgFenchat(){
	jQuery('.submit_msg').on('click',function(){
		sendMSG();
	});
	jQuery('.msg_fenchat').on('keyup',function(event){
		var key = event.keyCode || event.which;
		if(key===13){
			sendMSG();
		}
	}).on('keydown',function(event){
		var key = event.keyCode || event.which;
		if(key===9){
			event.preventDefault();
			var root = jQuery(this).val(),orig,rep,matches=[],exp,msg=root;
			msg = msg.split(' ');
			msg = msg[msg.length-1];
			orig = msg;
			msg = msg.toUpperCase();
			for(var i=0;i<channelList.length;i++){
				exp = new RegExp('('+msg+')');
				if(exp.test(channelList[i].toUpperCase())){
					matches.push(channelList[i]);
				}
			}
			if(matches.length>0){
				rep = matches.sort(function(a,b){return b.length-a.length;})[0];
			}else{
				rep = orig;
			}
			console.log(rep,orig);
			jQuery(this).val(root.replace(orig,rep+', '));
		}
	})
}
jQuery(document).ready(function(){
	socket.on('active-users',function(data){
		jQuery('.active-users').text(data.users+' Online');
	});
	socket.on('channel-listing',function(data){
		var channelListing = '';
		channelList=[];
		for(var index in data){
			channelList.push(index);
			if(data.hasOwnProperty(index)){
				if(data[index]=='~'){
					channelListing += '<div style="color:red">'+index+'</div>';
				}else if(data[index]=='%'){
					channelListing += '<div style="color:green">'+index+'</div>';
				}else{
					channelListing += '<div>'+index+'</div>';
				}
			}
		}
		jQuery('.channel-listing').html(channelListing);
	});
	socket.on('fenchat-connected',function(data){
		nick = data.nick;
		jQuery('.msg_fenchat').attr('placeholder','Type Message');
		msgFenchat();
		jQuery('.submit_msg').text('Send');
	});
	socket.on('fenchat-message',function(data){
		var message = '<tr>';
		message += '<td class="time"><div>'+moment().format('h:mm:ss a')+'</div></td>';
		switch(data.type){
			case 'message':
				data.color=false; //colors disabled
				message += '<td class="nick">'+data.nick+'</td>';
				message += '<td class="message"'+(data.color!==false?' style="color:'+colors[data.color]+'"':'')+'>'+replaceURLWithHTMLLinks(data.text)+'</td>';
				break;
			case 'emote':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.nick+' '+replaceURLWithHTMLLinks(data.text)+'</td>';
				break;
			case 'part':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.nick+' has parted ('+(data.reason||'')+')</td>';
				break;
			case 'kick':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.nick+' was kicked by '+data.by+' ('+(data.reason||'')+')</td>';
				break;
			case 'quit':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.nick+' has quit ('+(data.reason||'')+')</td>';
				break;
			case 'nick':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.oldnick+' is now known as '+data.newnick+'</td>';
				break;
			case 'join':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.nick+' has joined</td>';
				break;
			case 'notice':
				message += '<td class="nick">*</td>';
				message += '<td class="message">'+data.text+'</td>';
				break;
		}
		message += '</tr>';
		jQuery('.channel-text table tbody').prepend(message);
	});
	joinFenchat();
});