jQuery(document).ready(function(){
	jQuery('.stream-frame').css('height',jQuery('#container').height());
	jQuery('.hide-chat').on('click',function(event){
		event.preventDefault();
		var fillWidth = jQuery(window).width();
		if(jQuery('#fenchat').hasClass('hidden')){
			jQuery('#fenchat').removeClass('hidden').css('right',0);
			jQuery(this).text('Hide Chat');
		}else{
			jQuery('#fenchat').addClass('hidden').css('right',(-fillWidth)+'px');
			jQuery(this).text('Show Chat');
		}
	});
	jQuery('.list-header>div').on('click',function(event){
		event.preventDefault();
		if(!jQuery(this).hasClass('hidden')){
			jQuery('.list-header .active').removeClass('active');
			jQuery('.list-body .active').removeClass('active');
			jQuery(this).addClass('active');
			jQuery('.list-body>div[data-tab="'+jQuery(this).attr('data-tab')+'"]').addClass('active');
		}
	});
});