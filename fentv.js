var chatexp={
	emote:new RegExp('^\/me')
};

var express = require('express'),
	fs = require('fs'),
	irc = require('irc');

//globals
var app = express(),
	server = app.listen(8081),
	io = require('socket.io').listen(server, { log: false }),
	version = '0.0.1';

app.configure(function(){
	app.set('title', 'Fentv');
	app.set('views',__dirname);
	app.set('view engine','jade');
	app.use(express.static(__dirname+'/public'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(app.router);
});

app.get('/',function(req,res){
	res.render('index',{title:'Fentv'});
});

var activeUsers = 0;

function getColor(message){
	var codes = {
	    white: '\\u00030',
	    black: '\\u00031',
	    dark_blue: '\\u00032',
	    dark_green: '\\u00033',
	    light_red: '\\u00034',
	    dark_red: '\\u00035',
	    magenta: '\\u00036',
	    orange: '\\u00037',
	    yellow: '\\u00038',
	    light_green: '\\u00039',
	    cyan: '\\u000310',
	    light_cyan: '\\u000311',
	    light_blue: '\\u000312',
	    light_magenta: '\\u000313',
	    gray: '\\u000314',
	    light_gray: '\\u000315',
	    reset: '\\u000f',
	},exp,msg=message,code=false;
	for(var index in codes){
		if(codes.hasOwnProperty(index)){
			exp = new RegExp('('+codes[index]+')');
			if(exp.test(message)){
				code = index;
				msg = message.replace(exp,'');
			}
		}
	}
	return {message:msg,code:code};
}
function checkCode(message){
	var codes = {
	    normal: '\\u0001'
	},exp,code;
	for(var index in codes){
		if(codes.hasOwnProperty(index)){
			exp = new RegExp('('+codes[index]+')');
			if(exp.test(message)){
				code = null;
				message = message.replace(exp,'');
				if(message.indexOf('VERSION')!==-1){
					message = message.replace(/VERSION/g,version);
					code = 'version';
				}
				if(message.indexOf('ACTION')!==-1){
					message = message.replace(/ACTION/g,'');
					code = 'emote';
				}
				return {message:message,code:code};
			}
		}
	}
	return {message:message,code:false};
}
function checkClientCode(message){
	var codes = {
		emote: '\/me '
	},exp,code;
	for(var index in codes){
		if(codes.hasOwnProperty(index)){
			exp = new RegExp('('+codes[index]+')');
			if(exp.test(message)){
				code = index;
				message = message.replace(exp,'');
				return {message:message,code:code};
			}
		}
	}
	return {message:message,code:false};
}

io.sockets.on('connection',function(socket){
	var fenchat=false;
	activeUsers++;
	io.sockets.emit('active-users',{users:activeUsers});
	socket.on('join-fenchat',function(data){
		fenchat = new irc.Client('irc.rizon.net', data.nick, {
		    channels: ['#fenchat'],
		    autoConnect: false
		});
		fenchat.connect(function(){
			console.log(data.nick+' connected to fenchat');
			socket.emit('fenchat-connected',{
				nick:data.nick
			});
		});
		fenchat.addListener('names',function(channel,nicks){
			socket.emit('channel-listing',nicks);
		});
		fenchat.addListener('part',function(channel,nick,reason,message){
			socket.emit('fenchat-message',{type:'part',nick:nick,reason:reason,message:message});
		});
		// fenchat.addListener('quit',function(nick,reason,channels,message){
		// 	socket.emit('fenchat-message',{type:'quit',nick:nick,reason:reason,message:message});
		// });
		fenchat.addListener('kick',function(channel,nick,by,reason,message){
			socket.emit('fenchat-message',{type:'kick',nick:nick,reason:reason,by:by,message:message});
		});
		fenchat.addListener('nick',function(oldnick,newnick,channels,message){
			socket.emit('fenchat-message',{type:'nick',oldnick:oldnick,newnick:newnick,message:message});
		});
		fenchat.addListener('raw',function(message){
			console.log(message);
			var color,code;
			switch(message.command){
				case 'PRIVMSG':
					color = getColor(message.args[1]);
					code = checkCode(color.message);
					console.log(code);
					socket.emit('fenchat-message',{
						type:(code.code=='emote'?'emote':'message'),
						nick:message.nick,
						to:message.args[0],
						text:code.message,
						color:color.code,
						code:code.code
					});
					break;
				case 'JOIN':
					socket.emit('fenchat-message',{
						type:'join',
						nick:message.nick,
						reason:message.args[0].replace(/quit: /g,'')
					});
					break;
				case 'QUIT':
					socket.emit('fenchat-message',{
						type:'quit',
						nick:message.nick,
						to:message.args[0]
					});
					break;
				case 'NOTICE':
					// socket.emit('fenchat-message',{
					// 	type:'notice',
					// 	nick:message.nick,
					// 	to:message.args[0],
					// 	text:message.args[1]
					// });
					break;
			}
		});
		fenchat.addListener('error', function(message) {
		    console.log('error: ', message);
		});
	});
	socket.on('fenchat-message',function(data){
		var parse = checkClientCode(data.msg);
		if(parse.code=='emote'){
			fenchat.action('#fenchat',parse.message);
		}else{
			fenchat.say('#fenchat',data.msg);
		}
	});
	socket.on('disconnect',function(){
		activeUsers--;
		io.sockets.emit('active-users',{users:activeUsers});
		if(fenchat!==false){
			fenchat.disconnect('Fentv testing');
		}
	})
});